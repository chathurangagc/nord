# NORD SOFTWARE DEMO TEST

Create a page with a table that includes the following columns:
-
id
-
name
-
gender
-
age

Add a form to the page with the following inputs:
-
name (input, text)
-
gender (select)
-
age (input, number)
-
submit (button, submit)
When the form is submitted the table should be updated to include the new person.
Add a new column to the table with a link to delete a row. When the link is clicked the row should be removed from the table.

## Bonus
If you have time you can also add the following features:
-
Add data validation to the form inputs
-
Allow for sorting and filtering the table
-
Avoid using the JavaScript global scope
-
Add a CSS3 transition when altering the table
-
Use a CSS framework


## Talk Me
Gayan | Chathuranga
skype: chathurangagc
+94770190352
http://lk.linkedin.com/in/gayanc/

## License
Project is licensed not for free use
