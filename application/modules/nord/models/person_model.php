<?php
class Person_Model extends CI_Model
{
	
	public function __construct() {
        parent::__construct();
    }

	function getList(){
		$this->db->select('*');
        $this->db->from('nrd_person_data');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();  
        return $query->result_array();
	}

		/*
	Inserts or updates a item
	*/
	function save($data)
	{

			if($this->db->insert('nrd_person_data',$data))
			{
				return true;
			}
			return false;

	}
	

}
?>
