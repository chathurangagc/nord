<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nord extends MX_Controller { 


function __construct()
    {
        parent::__construct();
        $this->load->model('Person_Model');	
    }

	public function index(){
		 $this->load->view('form.tpl');
	}

	public function post(){
		$this->save();
		$data = array(
			'data_list' => $this->Person_Model->getList()
		);
		$content  = $this->load->view('listing.tpl',$data,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
	}

	public function view(){
		$data = array(
			'data_list' => $this->Person_Model->getList()
		);
		$content  = $this->load->view('listing.tpl',$data,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
	}

	public function save(){
		$person_data = array(
		'id' =>'',
		'name'=>$this->input->post('name'),
		'age'=>$this->input->post('age'),
		'gender'=>$this->input->post('gender'),
		);
		return $this->Person_Model->save($person_data);

	}

	
}
