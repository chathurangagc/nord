      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-body ">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">

               <table cellpadding="0" cellspacing="0" border="0" class="table table-striped dataTable" id="example2" width="100%" aria-describedby="example2_info" style="width: 100%;">
                <thead>
                  <tr role="row">
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" style="width: 165px;">ID</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 218px;">Name</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 204px;">Age</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 141px;">Gender</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 101px;">Operations</th></tr>
                </thead>
                
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                   <?php 
              foreach($data_list as $row => $dataRow) { 
             ?>
              <tr class="gradeX even rm_<?=$dataRow['id']?>" id="<?=$dataRow['id']?>">
                    <td class=" "><?=$dataRow['id']?></td>
                    <td class=" "><?=$dataRow['name']?></td>
                    <td class=" "><?=$dataRow['age']?></td>
                    <td class="center "><?=$dataRow['gender']?></td>
                    <td class="center ">
                    <a href="#"><i class="icon-edit"></i></a>
                    <a href="#"><i class="icon-align-justify"></i></a>    
                    <a href="#"><i class="icon-trash"></i></a>
                    </td>
              </tr>
              <?php }?>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>

<script src="<?=CDN?>plugins/jquery-datatable/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-datatable/extra/js/TableTools.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=CDN?>plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="<?=CDN?>plugins/datatables-responsive/js/lodash.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?=CDN?>js/datatables.js" type="text/javascript"></script>