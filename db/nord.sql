-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 07, 2014 at 10:38 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nord`
--

-- --------------------------------------------------------

--
-- Table structure for table `nrd_person_data`
--

CREATE TABLE IF NOT EXISTS `nrd_person_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('male','female') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='person data' AUTO_INCREMENT=24 ;

--
-- Dumping data for table `nrd_person_data`
--

INSERT INTO `nrd_person_data` (`id`, `name`, `age`, `gender`) VALUES
(1, 'sdfsdf', 234, 'male'),
(2, 'sdf', 234, 'male'),
(3, 'sdf', 234, 'male'),
(4, 'erter', 345, 'female'),
(5, 'dfgd', 234, 'male'),
(6, 'sdfsdf', 234, 'male'),
(7, 'ertert', 234, 'male'),
(8, 'fsfs', 34234, 'female'),
(9, 'fsfs', 34234, 'female'),
(10, 'sdfs', 234, 'female'),
(11, 'sdfs', 234, 'female'),
(12, '345', 234, 'male'),
(13, '345', 234, 'male'),
(14, '345', 234, 'male'),
(15, '345', 234, 'male'),
(16, '345', 234, 'female'),
(17, 'sdf', 34, 'male'),
(18, 'sdf', 35, 'male'),
(19, 'sdf', 35, 'male'),
(20, 'sdf', 35, 'male'),
(21, 'sdf', 35, 'male'),
(22, 'sdf', 35, 'male'),
(23, 'sdf', 35, 'male');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
