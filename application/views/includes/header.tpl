<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav"> 
      <!-- BEGIN TOP NAVIGATION MENU -->
	  <div class="pull-left"> 

		  <ul class="nav quick-section">
		  	<li class="quicklinks"> <h4><b>NORD</b> Software </h4></li>
		  	<li></li>
		  	<li>&nbsp;</li>
			<li class="quicklinks"> <h5><b>Today is </b><?php echo date('l jS \of F Y ');?></h5></li>        
		  </ul>
	  </div>
	 <!-- END TOP NAVIGATION MENU -->
	 <!-- BEGIN CHAT TOGGLER -->
      <div class="pull-right"> 
		<div class="chat-toggler">	
					<div class="user-details"> 
						<div class="username">
							<span class="badge badge-important">Dummy</span> 
							Hi <span class="bold">Niklas</span>									
						</div>						
					</div> 
					<div class="iconset top-down-arrow"></div>
				</a>						
				<div class="profile-pic"> 
					<img alt="" src="http://www.nordsoftware.com/sites/default/files/styles/front_personel_lift/public/niklas_1.jpg?itok=Bxz56xyF" data-src="http://www.nordsoftware.com/sites/default/files/styles/front_personel_lift/public/niklas_1.jpg?itok=Bxz56xyF" data-src-retina="http://www.nordsoftware.com/sites/default/files/styles/front_personel_lift/public/niklas_1.jpg?itok=Bxz56xyF" width="35" height="35" /> 
				</div>       			
			</div>
      </div>
	   <!-- END CHAT TOGGLER -->
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
   
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
