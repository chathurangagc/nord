<?php require_once(FRONT_LAYOUT_VIEW_PATH .'html.tpl'); ?>
<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'header.tpl'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'sidebar.tpl'); ?>
  <!-- END SIDEBAR -->
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="clearfix"></div>
    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3><span class="semi-bold">SIM</span>  Master Data</h3>
      </div>
     
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple">
            <div class="grid-title no-border">
            
            </div>
            <div class="grid-body no-border">
      <form class="form-no-horizontal-spacing" id="form-condensed" /> 
              <div class="row-fluid column-seperation">
                <div class="span6">
                  <h4>Supplier Information</h4>            
                    <div class="row-fluid"  style="margin-bottom: 10px;">
                      <div class="span6">
                       <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="Supplier"/>
                      </div>
                      <div class="span4">
                          <input name="form3Address" id="form3Address" type="text" class="span12" placeholder="Package" />                        
                      </div>
                      <div class="span2">
                                                
                      </div>
                    </div>
                    <div class="row-fluid" style="margin-bottom: 10px;">
                      <div class="span8">
                       <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="SIM No" />                          
                      </div>
                      <div class="span2">
                        
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span5">
                           <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="Purchased Price" />
                      </div>
                       <div class="span5">
                           <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="Selling Price" />
                      </div>
                    </div>
                    <div class="row-fluid small-text"> </div>

                </div>
                <div class="span6">
        
                   <h4>Customer Information</h4>
                
                     <div class="row-fluid">
                      <div class="span6">
                        <input type="text" class="span12" id="dpicker" placeholder="Cusomter NIC"/>
                      </div>
                      <div class="span6">
                      <input type="text" class="span12" id="dpicker" placeholder="Cusomer Name"/>                    
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                       <input name="form3Address" id="form3Address" type="text" class="span12" placeholder="Address Line 1" />
                      </div>
                    </div>
                     <div class="row-fluid">
                      <div class="span12">
                        <input name="form3Country" id="form3Country" type="text" class="span12" placeholder="Address Line 2" />
                      </div>
                    </div>
                     <div class="row-fluid">
                      <div class="span12">
                        <input name="form3Country" id="form3Country" type="text" class="span12" placeholder="Address Line 3" />
                      </div>
                    </div>
                   
             
                </div>
              </div>
              <div class="row-fluid column-seperation">
                <div class="span6">   
                <h4>Sale & Activation</h4>       
                    <div class="row-fluid"  style="margin-bottom: 10px;">
                      <div class="span6">
                            <input type="text" class="span12" id="dpicker" placeholder="Sale Person"/>
                      </div>
                      <div class="span4">
                        
                                                                    <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker" placeholder="Handed to Sale Person"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div> 
                      </div>
                      <div class="span2">
                                                
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row-fluid"  style="margin-bottom: 10px;">
                      <div class="span10">
                            <input type="text" class="span12" id="dpicker" placeholder="Sub Dealer"/>
                      </div>

                      <div class="span2">
                                                
                      </div>
                    </div>
                    <div class="row-fluid">                      
                      <div class="span4">
                       <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="Invoice No"/>               
                      </div>
                      <div class="span4">
                        
                        <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker" placeholder="Invoiced On"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div> 
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span4">
                       <input name="form3DateOfBirth" id="form3DateOfBirth" type="text" class="span12" placeholder="Activated On"/>               
                      </div>
                       <div class="span6">
                        <input name="form3Address" id="form3Address" type="text" class="span12" placeholder="Activated IVR" />
                      </div>
                    </div>
                    <div class="row-fluid small-text"> </div>

                </div>
                <div class="span6">
                <h4>Contract Information</h4>
                <div class="row-fluid">                      
                      <div class="span4">
                       <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker" placeholder="Sent On"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div>                
                      </div>
                      <div class="span2"></div>
                      <div class="span4">
                        
                        <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker" placeholder="Received On"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div> 
                      </div>
                    </div>
                </div>
              </div>
        <div class="form-actions">
          <div class="pull-left">

          </div>
          <div class="pull-right">
            <button class="btn btn-danger btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
            <button class="btn btn-white btn-cons" type="button">Cancel</button>
          </div>
          </div>
      </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END PAGE -->
<!-- BEGIN CHAT -->
<!-- END CHAT -->
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
 <?php require_once(FRONT_LAYOUT_VIEW_PATH .'corejs_assets.tpl'); ?>
<!-- END CORE JS FRAMEWORK -->


<!-- BEGIN PAGE LEVEL JS -->
<script src="<?=CDN?>plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
<script src="<?=CDN?>plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?=CDN?>plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-autonumeric/autoNumeric.js" type="text/javascript"></script>
<scr

<script src="<?=CDN?>plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=CDN?>js/form_validations.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'coretpl_js.tpl'); ?>
<!-- END CORE TEMPLATE JS -->
<!-- END JAVASCRIPTS -->
</body>