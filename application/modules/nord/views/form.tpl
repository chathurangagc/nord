<?php require_once(FRONT_LAYOUT_VIEW_PATH .'html.tpl'); ?>
<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'header.tpl'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'sidebar.tpl'); ?>
  <!-- END SIDEBAR -->
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="clearfix"></div>
    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3><span class="semi-bold">Personal</span>  Information</h3>
      </div>
      <div class="row-fluid appnd_list">
        <div class="span12">
          <div class="grid simple">
            <div class="grid-title no-border">
                     
            </div>
            <div class="grid-body no-border">
      <form class="form-no-horizontal-spacing" id="nord_form" /> 
              <div class="row-fluid column-seperation">
                <div class="span12">
        
                 <h4>Basic Info</h4>
                                        
                    <div class="row-fluid">
                    <div class="span4">
                      <input type="text" class="span12" name="name" id="name" placeholder="(Name) Kumar Sangakara"/>                  
                      </div>
                    </div>
                    <div class="row-fluid">
                       <div class="span8">
                           <input type="text" class="span4" name="age" id="age" placeholder="(Age) 25 Years"/>
                      </div>
                      <div class="span2" id="age"></div>
                    </div>
                    <div class="row-fluid">
                      <div class="span4">
                        <div class="radio">
                        <input type="radio" checked="checked" value="male" name="gender" id="male">
                        <label for="male">Male</label>
                        <input type="radio" value="female" name="gender" id="female">
                        <label for="female">Female</label>
                      </div>
                      </div>
                      <div class="span4">
                      </div>
                      <div class="span4" ></div>
                    </div>
                 
             
                </div>
              </div>
        <div class="form-actions">
          <div class="pull-left">
          </div>
          <div class="pull-right">
             <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Submit</button>
          </div>
          </div>
      </form>
            </div>
          </div>
        </div>
        <div class="tmp"></div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE -->
<!-- BEGIN CHAT -->
<!-- END CHAT -->
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
 <?php require_once(FRONT_LAYOUT_VIEW_PATH .'corejs_assets.tpl'); ?>
<!-- END CORE JS FRAMEWORK -->


<!-- BEGIN PAGE LEVEL JS -->
<script src="<?=CDN?>plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
<script src="<?=CDN?>plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="<?=CDN?>plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?=CDN?>plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS
<script src="<?=CDN?>js/form_validations.js" type="text/javascript"></script> -->
<script type="text/javascript">
  $(document).ready(function() {  
                     
                      $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('app/load'); ?>",
                          //dataType: 'json',
                          data: '',
                          success: function (data) {
                              $('.tmp').append(data.innerHTML);
                          }
                      });


          $('#nord_form').validate({
                // errorElement: 'span', 
                errorClass: 'error', 
                focusInvalid: true, 
                rules: {
                    name: {
                        required: true
                    },
                    age: {
                        required: true,
                        number: true
                    }
                },

                invalidHandler: function (event, validator) {
          //display error alert on form submit    
                },

                errorPlacement: function (label, element) { // render error placement for each input type   
                    $('<span class="error"></span>').insertAfter(element).append(label)
                },

                highlight: function (element) { // hightlight error inputs
          
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {

                },

                submitHandler: function (form) {


                     $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('app/post'); ?>",
                          //dataType: 'json',
                          data: $("#nord_form").serialize(),
                          success: function (data) {
                              console.log(data);
                             // $('.tmp').empty().remove();
                              //$(".appnd_list").load(data.innerHTML);

                              $('appnd_list').load(base_url('app/load'));
                          }
                      });
                }
            }); 
  });

</script>
<!-- BEGIN CORE TEMPLATE JS -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'coretpl_js.tpl'); ?>
<!-- END CORE TEMPLATE JS -->
<!-- END JAVASCRIPTS -->
</body>