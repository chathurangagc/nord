<?php require_once(FRONT_LAYOUT_VIEW_PATH .'html.tpl'); ?>
<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'header.tpl'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'sidebar.tpl'); ?>
  <!-- END SIDEBAR -->
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="clearfix"></div>
    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3><span class="semi-bold">SIM</span>  Stock Search</h3>
      </div>  
      

            <div class="row-fluid">
        <div class="span12">
          <div class="grid simple">
            <div class="grid-title no-border">
            
            </div>
            <div class="grid-body no-border">

              <form>
                <div class="row-fluid">
                  <div class="span4">
                    <input type="text" aria-controls="example2" class="input-medium span10" placeholder="Search By SIM Number">
                  </div>
                  <div class="span4"> <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker" placeholder="Date From"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div></div>
                  <div class="span4"> <div class="input-append success date">
                        <input type="text" class="span12" id="dpicker2" placeholder="Date To"/>
                        <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span> 
                        </div></div>                  
                  </div>
                  <div class="form-actions">
          <div class="pull-left">

                      <div class="checkbox check-success">
                        <input id="checkbox2" type="checkbox" value="1" checked="checked">
                        <label for="checkbox2">With Customer</label>
                        <input id="checkbox2" type="checkbox" value="1" checked="checked">
                        <label for="checkbox2">With Sub Dealer</label>
                        <input id="checkbox2" type="checkbox" value="1" checked="checked">
                        <label for="checkbox2">With Sale Person</label>
                        <input id="checkbox2" type="checkbox" value="1" checked="checked">
                        <label for="checkbox2">Stock Pending Approval</label>
                        <input id="checkbox2" type="checkbox" value="1" checked="checked">
                        <label for="checkbox2">With Sale Person</label>
                      </div>
          </div>
          <div class="pull-right">
            <button class="btn btn-danger btn-cons" type="submit"><i class="icon-search"></i> Search</button>
          </div>
          </div>
              </form>
                </div>
                </div>
                </div>
      </div>

      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
<!--             <div class="grid-title">
              <h4>Etisalat <span class="semi-bold">Broad Band Post Paid</span></h4>
            </div> -->
            <div class="grid-body ">
              	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped dataTable" id="example2" width="100%" aria-describedby="example2_info" style="width: 100%;">
                <thead>
                  <tr role="row">
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label=": activate to sort column ascending" style="width: 22px;"></th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" style="width: 165px;">SIM Number</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 218px;">Selling Price (Rs)</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 204px;">Purchasing</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 141px;">Engine version</th>
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 101px;">CSS grade</th></tr>
                </thead>
                
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                 <?php 
              for($i=0; $i<100; $i++) { 
                $simNo =  str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT);
                $sellPrice=  str_pad(mt_rand(1,99999999),3,'0',STR_PAD_LEFT);
                $purchPrice=  str_pad(mt_rand(1,99999999),3,'0',STR_PAD_LEFT);
             ?>
              <tr class="gradeX even">
                    <td class="center "><i class="icon-plus-sign"></i></td>
                    <td class=" ">072 - <?=$simNo?></td>
                    <td class=" "><?=$sellPrice?></td>
                    <td class=" "><?=$purchPrice?></td>
                    <td class="center ">With Customer</td>
                    <td class="center ">
                    <a href="#"><i class="icon-edit"></i></a>
                    <a href="#"><i class="icon-align-justify"></i></a>    
                    <a href="#"><i class="icon-trash"></i></a>
                    </td>
              </tr>
              <?php }?>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE -->
<!-- BEGIN CHAT -->
<!-- END CHAT -->
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
 <?php require_once(FRONT_LAYOUT_VIEW_PATH .'corejs_assets.tpl'); ?>
<!-- END CORE JS FRAMEWORK -->


<!-- BEGIN PAGE LEVEL JS -->
<script src="<?=CDN?>plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
<script src="<?=CDN?>plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?=CDN?>plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-autonumeric/autoNumeric.js" type="text/javascript"></script>


<script src="<?=CDN?>plugins/jquery-datatable/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=CDN?>plugins/jquery-datatable/extra/js/TableTools.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=CDN?>plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="<?=CDN?>plugins/datatables-responsive/js/lodash.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?=CDN?>js/datatables.js" type="text/javascript"></script>



<script src="<?=CDN?>plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=CDN?>js/form_validations.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<?php require_once(FRONT_LAYOUT_VIEW_PATH .'coretpl_js.tpl'); ?>
<!-- END CORE TEMPLATE JS -->
<!-- END JAVASCRIPTS -->
</body>